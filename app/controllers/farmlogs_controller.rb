class FarmlogsController < ApplicationController
  before_action :set_farmlog, only: [:show, :edit, :update, :destroy]

  # GET /farmlogs
  # GET /farmlogs.json
  def index
    @farmlogs = Farmlog.all
  end

  # GET /farmlogs/1
  # GET /farmlogs/1.json
  def show
  end

  # GET /farmlogs/new
  def new
    @farmlog = Farmlog.new
  end

  # GET /farmlogs/1/edit
  def edit
  end

  # POST /farmlogs
  # POST /farmlogs.json
  def create
    @farmlog = Farmlog.new(farmlog_params)

    respond_to do |format|
      if @farmlog.save
        format.html { redirect_to @farmlog, notice: 'Farmlog was successfully created.' }
        format.json { render :show, status: :created, location: @farmlog }
      else
        format.html { render :new }
        format.json { render json: @farmlog.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /farmlogs/1
  # PATCH/PUT /farmlogs/1.json
  def update
    respond_to do |format|
      if @farmlog.update(farmlog_params)
        format.html { redirect_to @farmlog, notice: 'Farmlog was successfully updated.' }
        format.json { render :show, status: :ok, location: @farmlog }
      else
        format.html { render :edit }
        format.json { render json: @farmlog.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /farmlogs/1
  # DELETE /farmlogs/1.json
  def destroy
    @farmlog.destroy
    respond_to do |format|
      format.html { redirect_to farmlogs_url, notice: 'Farmlog was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_farmlog
      @farmlog = Farmlog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def farmlog_params
      params.require(:farmlog).permit(:title, :user_id, :img, :picture, :picture_cache)
    end
end
