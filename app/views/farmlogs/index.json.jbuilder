json.array!(@farmlogs) do |farmlog|
  json.extract! farmlog, :id, :title, :user_id, :img
  json.url farmlog_url(farmlog, format: :json)
end
