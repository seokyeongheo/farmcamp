require 'test_helper'

class FarmlogsControllerTest < ActionController::TestCase
  setup do
    @farmlog = farmlogs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:farmlogs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create farmlog" do
    assert_difference('Farmlog.count') do
      post :create, farmlog: { img: @farmlog.img, title: @farmlog.title, user_id: @farmlog.user_id }
    end

    assert_redirected_to farmlog_path(assigns(:farmlog))
  end

  test "should show farmlog" do
    get :show, id: @farmlog
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @farmlog
    assert_response :success
  end

  test "should update farmlog" do
    patch :update, id: @farmlog, farmlog: { img: @farmlog.img, title: @farmlog.title, user_id: @farmlog.user_id }
    assert_redirected_to farmlog_path(assigns(:farmlog))
  end

  test "should destroy farmlog" do
    assert_difference('Farmlog.count', -1) do
      delete :destroy, id: @farmlog
    end

    assert_redirected_to farmlogs_path
  end
end
