class CreateFarmlogs < ActiveRecord::Migration
  def change
    create_table :farmlogs do |t|
      t.string :title
      t.references :user, index: true, foreign_key: true
      t.string :img

      t.timestamps null: false
    end
  end
end
